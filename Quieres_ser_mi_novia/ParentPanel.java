package Quieres_ser_mi_novia;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.BorderFactory;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.border.Border;
import javax.swing.SwingConstants;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ParentPanel extends JPanel {

    private static Color BLUE_COLOR = Color.decode("#079992");
    private static Color PANEL_BLUE_COLOR = Color.decode("#0abde3");
    private static Color GREEN_COLOR = Color.decode("#4cd137");
    private static Font MY_FONT = new Font("", Font.BOLD, 22);
    private JLabel prompt = new JLabel("¿QUIERES SER MI NOVIA?");
    private JLabel thanks = new JLabel("<html><center>Sabía que dirías <br> que si jijiji</center></html>");
    private JButton yes = new JButton("SI");
    private JButton no = new JButton("NO");
    private static Border BORDER = BorderFactory.createLineBorder(Color.BLACK, 3);

    public ParentPanel(){
        setLayout(null);
        setBackground(ParentPanel.PANEL_BLUE_COLOR);

        add(prompt);
        prompt.setBounds(80, 20, 400, 100);
        prompt.setFont(ParentPanel.MY_FONT);

        add(thanks);
        thanks.setBounds(150, 130, 160, 90);
        thanks.setBorder(ParentPanel.BORDER);
        thanks.setHorizontalAlignment(SwingConstants.CENTER);
        thanks.setOpaque(true);
        thanks.setVisible(false);

        add(yes);
        yes.setBounds(80, 280, 80, 40);
        yes.setBackground(ParentPanel.BLUE_COLOR);
        yes.setFont(ParentPanel.MY_FONT);
        yes.setBorder(ParentPanel.BORDER);
        yes.addMouseListener(new ChangeColorButton(yes));
        yes.addActionListener(new ShowPrompt());

        add(no);
        no.setBounds(310, 280, 80, 40);
        no.setFont(ParentPanel.MY_FONT);
        no.setBackground(ParentPanel.BLUE_COLOR);
        no.setBorder(ParentPanel.BORDER);
        no.addMouseListener(new ChangeColorButton(no));
    }


    private class ChangeColorButton implements MouseListener {
        private JButton button;

        public ChangeColorButton(JButton b){
            button = b;
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            // TODO Auto-generated method stub
        }

        @Override
        public void mousePressed(MouseEvent e) {
            // TODO Auto-generated method stub 
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            // TODO Auto-generated method stub
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            if(button.getText().equals("NO")){
                int x_location = (int) (Math.random()*400);
                int y_location = (int) (Math.random()*350);

                button.setLocation(x_location, y_location);
            }
            else {
                button.setBackground(ParentPanel.GREEN_COLOR);  
            }
        }

        @Override
        public void mouseExited(MouseEvent e) {
            button.setBackground(ParentPanel.BLUE_COLOR);
        }    
    }

    private class ShowPrompt implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            thanks.setVisible(true);            
        }
    
        
    }
}
